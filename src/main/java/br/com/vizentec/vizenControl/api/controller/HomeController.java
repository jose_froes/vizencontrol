package br.com.vizentec.vizenControl.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.vizentec.vizenControl.api.VizenControlApplication;

@Controller
public class HomeController {

	@GetMapping("/")
	public String home(VizenControlApplication application){
		return "dashboard";
	}
}
