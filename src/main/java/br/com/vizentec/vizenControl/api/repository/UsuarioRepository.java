package br.com.vizentec.vizenControl.api.repository;

import java.util.Optional;

import br.com.vizentec.vizenControl.api.model.Usuario;

public interface UsuarioRepository extends VizenControlBaseRepository<Usuario>{

	Optional<Usuario> findByIdDispositivoAndNome(String idDispositivo, String nome);

	Optional<Usuario> findByIdDispositivo(String idDispositivo);

}
