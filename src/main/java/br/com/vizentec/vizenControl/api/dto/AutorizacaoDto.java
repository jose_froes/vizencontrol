package br.com.vizentec.vizenControl.api.dto;

import lombok.Data;

@Data
public class AutorizacaoDto {

	private String idDispositivo;
	private String data;
	private String nome;
}
