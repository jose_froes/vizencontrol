package br.com.vizentec.vizenControl.api.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class MovimentoDto {

	private String idDispositivo;
	private String idProduto;
	private String data;
	private BigDecimal valor;
}
