package br.com.vizentec.vizenControl.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VizenControlApplication {

	public static void main(String[] args) {
		SpringApplication.run(VizenControlApplication.class, args);
	}
}
