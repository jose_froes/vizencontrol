package br.com.vizentec.vizenControl.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.vizentec.vizenControl.api.model.EntidadeBase;

public interface VizenControlBaseRepository<E extends EntidadeBase<E>> extends JpaRepository<E, Long>{
}
