package br.com.vizentec.vizenControl.api.util;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.text.MaskFormatter;

import org.springframework.util.StringUtils;

public class Util {
	
	public static final String HH_MM_SS = "HH:mm:ss";
	public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	
	public static final DateTimeFormatter tf = DateTimeFormatter.ofPattern(HH_MM_SS);
	public static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(DD_MM_YYYY);
	public static final DateTimeFormatter dtfComp = DateTimeFormatter.ofPattern(DD_MM_YYYY_HH_MM_SS);

	public static String formatarValor(BigDecimal valor) {
		if(valor != null) {
			NumberFormat formatter = NumberFormat.getCurrencyInstance();
			return formatter.format(valor);			
		}
		return "0,00";
	}
	
	public static String formatarDataTime(LocalDateTime dateTime) {
		if(dateTime != null) {
			return dtfComp.format(dateTime);
		}
		return "";
	}
	
	public static String formatarData(LocalDate date) {
		if(date != null) {
			return dtf.format(date);	
		}
		return "";
	}
	
	public static String formatCpfOuCnpj(String cpfOuCnpj) {
		if(!StringUtils.isEmpty(cpfOuCnpj)) {
			if(cpfOuCnpj.length() > 11) {
				return formatCnpj(cpfOuCnpj);
			} else {
				return formatCpf(cpfOuCnpj);
			}			
		} else {
			return "";
		}
	}
	
	public static String formatCnpj(String cnpj){
//		String cnpj = "07923215000123";
		try {
		    MaskFormatter mask = new MaskFormatter("##.###.###/####-##");
		    mask.setValueContainsLiteralCharacters(false);
		    return mask.valueToString(cnpj);
		} catch (ParseException ex) {
		    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "";				
	}
	
	public static String formatCpf(String cpf) {
//		String cnpj = "71960678191";
		try {
			MaskFormatter mask = new MaskFormatter("###.###.###-##");
			mask.setValueContainsLiteralCharacters(false);
			return mask.valueToString(cpf);
		} catch (ParseException ex) {
			Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "";				
	}

	public static String formatPlaca(String placa) {
//		String cnpj = "71960678191";
		if(!StringUtils.isEmpty(placa)) {
			try {			
				return placa.substring(0, 3)+"-"+placa.substring(3,placa.length());
			} catch (Exception ex) {			
				Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
			}			
		}
		return "";				
	}

	/**
	 *
	 * formata java Util date para o formato padrão "dd/MM/yyyy HH:mm:ss" ou passe o formato desejado.
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String formatarUtilData(Date date, String pattern) {
		if(date != null) {
			if(StringUtils.isEmpty(pattern)) {
				pattern = DD_MM_YYYY_HH_MM_SS;				
			}
			SimpleDateFormat sdfComp = new SimpleDateFormat(pattern);
			return sdfComp.format(date);	
		}
		return "";
	}

	public static LocalDate formatarDataByLocalDate(String date) {
		if(date != null) {
			return LocalDate.parse(date, dtf);
		}
		return null;
	}

	public static LocalDateTime formatarDataByLocalDateTime(String date) {
		if(date != null) {
			return LocalDateTime.parse(date, dtfComp);
		}
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(List.class);
	}
}
