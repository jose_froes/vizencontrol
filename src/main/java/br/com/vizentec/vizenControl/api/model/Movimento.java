package br.com.vizentec.vizenControl.api.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.vizentec.vizenControl.api.model.type.TipoMovimento;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tbMovimento")
@NoArgsConstructor
@AllArgsConstructor
public class Movimento extends EntidadeBase<Movimento> {
	
	@Builder
	public Movimento(LocalDateTime data, @NotBlank String idDispositivo, @NotBlank String idProduto, @NotNull BigDecimal valor, Pagamento pagamento, TipoMovimento tipoMovimento) {
		super(data);
		this.idDispositivo = idDispositivo;
		this.idProduto = idProduto;
		this.valor = valor;
		this.pagamento = pagamento;
		this.tipoMovimento = tipoMovimento;
	}

	@NotBlank
	private @Getter @Setter String idDispositivo;

	@NotBlank
	private @Getter @Setter String idProduto;

	@NotNull
	@Getter @Setter private BigDecimal valor;
	
	@ManyToOne
	@JoinColumn(name="id_pagamento")
	@Getter @Setter private Pagamento pagamento;

	@Enumerated(EnumType.STRING)
	@Getter @Setter private TipoMovimento tipoMovimento;
}
