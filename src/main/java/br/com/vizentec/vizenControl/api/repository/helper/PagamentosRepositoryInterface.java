package br.com.vizentec.vizenControl.api.repository.helper;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.vizentec.vizenControl.api.model.Pagamento;

public interface PagamentosRepositoryInterface {

	@Query("FROM Pagamento p inner join p.usuario u WHERE u.idDispositivo = :idDispositivo")
	public List<Pagamento> listarPagamentos(@Param("idDispositivo") String idDispositivo);
}
