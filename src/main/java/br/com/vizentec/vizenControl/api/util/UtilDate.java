package br.com.vizentec.vizenControl.api.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

public class UtilDate {

	/**
	 * Passar data e quantidade de meses que pode ser acrescentado um reduzido.
	 * Ex 1: 01/01/2020 se passar '-1' a data retornada deverá ser 01/12/2019
	 * Ex 2: 01/01/2020 se passar '2' a data retornada deverá ser 01/03/2020
	 * 
	 * @param data
	 * @param qtde
	 * @return
	 */
	public static Date diminuirAcrescentarMesDate(LocalDate data, Integer qtde) {
		return converterLocalDateUtilDate(data.plusMonths(qtde));
	}

	public static Date converterLocalDateUtilDate(LocalDate data) {
		return Date.from(data.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public static Date diminuirAcrescentarMesDateTime(LocalDateTime data, Integer qtde) {
		return converterLocalDateUtilDateTime(data.plusMonths(qtde));
	}
	
	public static Date converterLocalDateUtilDateTime(LocalDateTime data) {
		OffsetDateTime odt = OffsetDateTime.now ();
		ZoneOffset zoneOffset = odt.getOffset ();
		
		return Date.from(data.toInstant(zoneOffset));
	}
	
	public static LocalDateTime dataTimeInicio(LocalDateTime localDateTime) {
		
		localDateTime = localDateTime.withHour(0).withMinute(0).withSecond(0).withNano(0);
		
		return localDateTime;
	}
	
	public static LocalDateTime dataTimeFim(LocalDateTime localDateTime) {
		
		localDateTime = localDateTime.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
		
		return localDateTime;
	}
	
	public static void main(String[] args) {
		LocalDateTime teste = LocalDateTime.now();
		
		System.out.println(dataTimeInicio(teste));
		System.out.println(dataTimeFim(teste));
	}
}
