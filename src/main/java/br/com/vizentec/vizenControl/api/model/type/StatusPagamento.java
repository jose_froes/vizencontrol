package br.com.vizentec.vizenControl.api.model.type;

import lombok.Getter;

public enum StatusPagamento {

	APROVAR(1, "Aprovar"),
	APROVADO(2, "Aprovado"),
	NEGADO(3, "Negado");
	
	private @Getter Integer codigo;
	private @Getter String descricao;

	StatusPagamento(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
}
