package br.com.vizentec.vizenControl.api.repository;

import br.com.vizentec.vizenControl.api.model.Pagamento;
import br.com.vizentec.vizenControl.api.repository.helper.PagamentosRepositoryInterface;

public interface PagamentoRepository extends VizenControlBaseRepository<Pagamento>, PagamentosRepositoryInterface {
}
