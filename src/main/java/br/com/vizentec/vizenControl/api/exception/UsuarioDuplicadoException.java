package br.com.vizentec.vizenControl.api.exception;

public class UsuarioDuplicadoException extends Exception {

	private static final long serialVersionUID = -2995226155878323229L;

	public UsuarioDuplicadoException() {
		super();
	}

	public UsuarioDuplicadoException(String message) {
		super(message);
	}
}
