package br.com.vizentec.vizenControl.api.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PagamentoDto {
	
	private String idDispositivo;
	private BigDecimal valor;

}
