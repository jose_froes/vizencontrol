package br.com.vizentec.vizenControl.api.exception;

public class UsuarioNotFoundException extends Exception {

	private static final long serialVersionUID = 2716426966374024203L;

	public UsuarioNotFoundException(String message) {
		super(message);
	}
}
