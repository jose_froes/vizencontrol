package br.com.vizentec.vizenControl.api.model.type;

import lombok.Getter;

public enum TipoMovimento {

	API("Debito lançado pela API"),
	PAGAMENTO_APROVADO("Pagamento credito aprovado"),
	PAGAMENTO_NEGADO("Pagamento debitado por estar negado");
	
	@Getter private String descricao;
	
	TipoMovimento(String descricao){
		this.descricao = descricao;
	}
}
