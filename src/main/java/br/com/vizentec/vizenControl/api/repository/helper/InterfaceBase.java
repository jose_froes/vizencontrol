package br.com.vizentec.vizenControl.api.repository.helper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface InterfaceBase<E, D> {

	public Page<E> filtrar(D filtro, Pageable pageable);
}
