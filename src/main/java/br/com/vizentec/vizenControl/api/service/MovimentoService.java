package br.com.vizentec.vizenControl.api.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import br.com.vizentec.vizenControl.api.model.Movimento;
import br.com.vizentec.vizenControl.api.model.Pagamento;
import br.com.vizentec.vizenControl.api.model.type.TipoMovimento;
import br.com.vizentec.vizenControl.api.repository.MovimentacaoRepository;

@Service
public class MovimentoService extends BaseService<Movimento, MovimentacaoRepository>{	

	public Movimento criarMovimentoPagamento(Pagamento pag, BigDecimal valorExtorno, TipoMovimento tipoMovimento) {

		Movimento mov = Movimento.builder()
							.data(LocalDateTime.now())
							.idDispositivo(pag.getUsuario().getIdDispositivo())
							.idProduto("0")
							.valor(valorExtorno)
							.pagamento(pag)
							.tipoMovimento(tipoMovimento)
							.build();
		return repository.save(mov);
	}
}
