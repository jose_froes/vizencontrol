package br.com.vizentec.vizenControl.api.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tbUsuario")
@NoArgsConstructor
@AllArgsConstructor
public class Usuario extends EntidadeBase<Usuario> {

	@Builder
	public Usuario(@NotNull LocalDateTime data, @NotBlank String idDispositivo, @NotBlank String nome) {
		super(data);
		this.idDispositivo = idDispositivo;
		this.nome = nome;
	}

	@NotBlank
	@Column(unique = true)
	private @Getter @Setter String idDispositivo;
	
	@NotBlank
	private @Getter @Setter String nome;
	
}
