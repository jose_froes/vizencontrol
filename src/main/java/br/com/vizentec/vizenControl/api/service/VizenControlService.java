package br.com.vizentec.vizenControl.api.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.vizentec.vizenControl.api.exception.UsuarioDuplicadoException;
import br.com.vizentec.vizenControl.api.exception.UsuarioNotFoundException;
import br.com.vizentec.vizenControl.api.model.Movimento;
import br.com.vizentec.vizenControl.api.model.Pagamento;
import br.com.vizentec.vizenControl.api.model.Usuario;
import br.com.vizentec.vizenControl.api.model.type.StatusPagamento;
import br.com.vizentec.vizenControl.api.model.type.TipoMovimento;
import br.com.vizentec.vizenControl.api.repository.MovimentacaoRepository;
import br.com.vizentec.vizenControl.api.repository.PagamentoRepository;
import br.com.vizentec.vizenControl.api.repository.UsuarioRepository;

@Service
public class VizenControlService {
	
	@Autowired
	private MovimentacaoRepository movimentacaoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private PagamentoRepository pagamentoRepository;

	public List<Movimento> consultarExtrato(String idDispositivo, LocalDateTime dateTimeInicio, LocalDateTime dateTimeFinal) {
		
		List<Movimento> lista = movimentacaoRepository.findAllByIdDispositivoAndDataAfterAndDataBefore(idDispositivo.toString(), dateTimeInicio, dateTimeFinal);
		
		return lista;
	}

	public Movimento movimentacao(String idDispositivo, String idProduto, LocalDateTime dateTime, BigDecimal valorBigDecimal) {
		
		if(!usuarioRepository.findByIdDispositivo(idDispositivo).isPresent()) {
			throw new  RuntimeException("Dispositivo não autorizado!");
		}
		
		Movimento entidade = Movimento.builder()
								.idDispositivo(idDispositivo)
								.idProduto(idProduto)
								.data(dateTime)
								.valor(valorBigDecimal)
								.tipoMovimento(TipoMovimento.API)
								.build();
		
		return movimentacaoRepository.save(entidade);
	}

	public Usuario autorizacao(String idDispositivo, LocalDateTime dateTime, String nome) throws UsuarioDuplicadoException {

		if(usuarioRepository.findByIdDispositivo(idDispositivo).isPresent()) {
			throw new UsuarioDuplicadoException("Este dispositivo já está cadastrado!");
		}
		
		Usuario entidade = Usuario.builder()
							.idDispositivo(idDispositivo)
							.data(dateTime)
							.nome(nome)
							.build();
		
		
		return usuarioRepository.save(entidade);
	}

	public Pagamento pagamento(String idDispositivo, BigDecimal valorBigDecimal) throws UsuarioNotFoundException {
		
		Optional<Usuario> usuarioOpt = usuarioRepository.findByIdDispositivo(idDispositivo);
		
		if(!usuarioOpt.isPresent()) {
			throw new UsuarioNotFoundException("Usuário com este dispositivo não foi encontrado!");
		}
		
		Pagamento pagamento = Pagamento.builder()
				.usuario(usuarioOpt.get())
				.valor(valorBigDecimal)
				.data(LocalDateTime.now())
				.status(StatusPagamento.APROVAR)
				.build();
		
		return pagamentoRepository.save(pagamento);
	}
}