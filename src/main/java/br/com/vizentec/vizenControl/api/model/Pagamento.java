package br.com.vizentec.vizenControl.api.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import br.com.vizentec.vizenControl.api.model.type.StatusPagamento;
import br.com.vizentec.vizenControl.api.util.Util;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tbPagamento")
@NoArgsConstructor
@AllArgsConstructor
public class Pagamento extends EntidadeBase<Pagamento> {
	
	@Builder
	public Pagamento(@NotNull LocalDateTime data, @NotNull BigDecimal valor, @NotNull Usuario usuario, @NotNull StatusPagamento status) {
		super(data);
		this.valor = valor;
		this.usuario = usuario;
		this.status = status;
	}

	@NotNull(message = "Valor é obrigatório!")	
	@Getter @Setter private BigDecimal valor;
	
	@NotNull(message = "Usuário é para pagamento é obrigatório")
	@ManyToOne
	@JoinColumn(name="id_usuario")
	@Getter @Setter private Usuario usuario;

	@NotNull(message = "Status do pagamento é obrigatório")
	@Enumerated(EnumType.STRING)
	@Getter @Setter private StatusPagamento status;
	
	
	
	@Transient
	public String getDataFormatada() {
		return Util.formatarDataTime(getData());			
	}
	
	@Transient
	public String getValorFormatado() {
		return Util.formatarValor(getValor());
	}
	
}
