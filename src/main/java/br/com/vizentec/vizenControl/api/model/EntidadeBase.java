package br.com.vizentec.vizenControl.api.model;

import java.time.LocalDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class EntidadeBase<E> {
	
	public EntidadeBase(@NotNull LocalDateTime data) {
		this.data = data;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	@Getter @Setter private Long id;
	
	@NotNull
	@Getter @Setter private LocalDateTime data;
	
	public boolean getPersistente() {
		if(this.id == null || this.id <= 0) {
			return false;
		} 
		return true;
	}
}
