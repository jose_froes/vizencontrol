package br.com.vizentec.vizenControl.api.controller;

import java.math.BigDecimal;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.vizentec.vizenControl.api.dto.PagamentoDto;
import br.com.vizentec.vizenControl.api.model.Movimento;
import br.com.vizentec.vizenControl.api.model.Pagamento;
import br.com.vizentec.vizenControl.api.model.type.StatusPagamento;
import br.com.vizentec.vizenControl.api.model.type.TipoMovimento;
import br.com.vizentec.vizenControl.api.repository.PagamentoRepository;
import br.com.vizentec.vizenControl.api.service.MovimentoService;

@Controller
@RequestMapping("/pagamentos")
public class PagamentosController {

	@Autowired
	private PagamentoRepository pagamentos;
	
	@Autowired
	private MovimentoService movimentoService;
	
	@GetMapping
	public ModelAndView pesquisar(PagamentoDto pagamentoDto) {
		ModelAndView mv = new ModelAndView("pagamentos/PesquisaPagamentos");
		mv.addObject("pagamentoDto", new PagamentoDto());
		if(!StringUtils.isEmpty(pagamentoDto.getIdDispositivo())) {
			mv.addObject("pagamentos", pagamentos.listarPagamentos(pagamentoDto.getIdDispositivo()));			
		} else {
			mv.addObject("pagamentos", pagamentos.findAll());						
		}
		return mv;
	}
	
	@RequestMapping(value = { "/aprovar"}, method = RequestMethod.POST)
	public ModelAndView aprovar(Pagamento pagamento, BindingResult result, Model model, RedirectAttributes attributes) {
		Optional<Pagamento> optional = pagamentos.findById(pagamento.getId());
		if(optional.isPresent()) {
			Pagamento pag = optional.get();
			pag.setStatus(StatusPagamento.APROVADO);
			pag = pagamentos.save(pag);
			Movimento mov = movimentoService.criarMovimentoPagamento(pag, pag.getValor(), TipoMovimento.PAGAMENTO_APROVADO);
			attributes.addFlashAttribute("mensagem", "Pagamento Aprovado e lançado movimento "+mov.getId()+ " com sucesso!");
			return new ModelAndView("redirect:/pagamentos");
		}
		return new ModelAndView("/pagamentos");
	}

	@RequestMapping(value = { "/negar"}, method = RequestMethod.POST)
	public ModelAndView negar(Pagamento pagamento, BindingResult result, Model model, RedirectAttributes attributes) {
		Optional<Pagamento> optional = pagamentos.findById(pagamento.getId());
		if(optional.isPresent()) {
			Pagamento pag = optional.get();
			BigDecimal valorExtorno = BigDecimal.ZERO;
			if(pag.getStatus() == StatusPagamento.APROVADO) {
				valorExtorno = pag.getValor().multiply(new BigDecimal("-1"));
			}
			pag.setStatus(StatusPagamento.NEGADO);
			pag = pagamentos.save(pag);
			movimentoService.criarMovimentoPagamento(pag, valorExtorno, TipoMovimento.PAGAMENTO_NEGADO);
			attributes.addFlashAttribute("mensagemNegado", "Pagamento Negado!");
			return new ModelAndView("redirect:/pagamentos");
		}
		return new ModelAndView("/pagamentos");
	}
}
