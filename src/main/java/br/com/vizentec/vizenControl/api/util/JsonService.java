package br.com.vizentec.vizenControl.api.util;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

@Service
public class JsonService {
	
	public static <T> T toStringJsonByObject(Class<T> clazz, String jsonText) {
		Gson gson = criarGsonCustom();
		return gson.fromJson(jsonText, clazz);
	}
	
	public static <T> String toObjectByStringJson(T objeto) {
		Gson gson = criarGsonCustom();
		return gson.toJson(objeto);
	}

	protected static Gson criarGsonCustom() {
		Gson gson = new GsonBuilder()
				.registerTypeAdapter(LocalDateTime.class, new JsonDeserializer<LocalDateTime>(){
					@Override
                    public LocalDateTime deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                        JsonObject jo = json.getAsJsonObject();
                        JsonObject dateJsonObject = jo.get("date").getAsJsonObject();
                        JsonObject timeJsonObject = jo.get("time").getAsJsonObject();
                        return LocalDateTime.of(dateJsonObject.get("year").getAsInt(),
                        						dateJsonObject.get("month").getAsInt(),
                        						dateJsonObject.get("day").getAsInt(),
                        						timeJsonObject.get("hour").getAsInt(),
                        						timeJsonObject.get("minute").getAsInt(),
                        						timeJsonObject.get("second").getAsInt(),
                        						timeJsonObject.get("nano").getAsInt());
                    }
				})
				.registerTypeAdapter(LocalDate.class, new JsonDeserializer<LocalDate>(){
					@Override
                    public LocalDate deserialize(JsonElement json, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
                        JsonObject jo = json.getAsJsonObject();
                        return LocalDate.of(jo.get("year").getAsInt(),
			                        		jo.get("month").getAsInt(),
			                        		jo.get("day").getAsInt());
                    }
				})
				.create();
		return gson;
	}

	public static <T> String objectListToJson(List<T> lista) {

		try {

			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			return ow.writeValueAsString(lista);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static <T> String ojectToJson(T objeto) {

		try {

			ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
			return ow.writeValueAsString(objeto);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public static <T> T jsonToObject(Class<T> clazz, String json) {

		try {

			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(json, clazz);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static <T> List<T> jsonToList(Class<T> clazz, String json) {
		
		try {
			
			ObjectMapper mapper = new ObjectMapper();
			List<?> lista = mapper.readValue(json, List.class);
			List<T> listaObjRet = new ArrayList<T>();
			lista.forEach(item -> {		
				Map<String, Object> mapTemp = new HashMap<String, Object>();
				LinkedHashMap linked = (LinkedHashMap) item;
				linked.forEach((key, value) -> {
					mapTemp.put(key.toString(), value);
				});
				listaObjRet.add(Reflection.preencherEntidadeDinamicamente(clazz, mapTemp));				
			});
			
			return listaObjRet;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
