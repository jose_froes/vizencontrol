package br.com.vizentec.vizenControl.api.service;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.vizentec.vizenControl.api.model.EntidadeBase;
import br.com.vizentec.vizenControl.api.repository.VizenControlBaseRepository;

public abstract class BaseService<E extends EntidadeBase<E>, R extends VizenControlBaseRepository<E>> {

	@Autowired
	protected R repository;
}
