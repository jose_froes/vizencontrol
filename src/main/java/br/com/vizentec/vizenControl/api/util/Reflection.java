package br.com.vizentec.vizenControl.api.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Map;

import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Reflection {

	public static <T> String obterValorCampo(String nameCampo, T entidade){
        Class<?> classe = entidade.getClass();      
        Field[] campos = classe.getDeclaredFields();        
        for (Field campo : campos) {
        	if(campo.getName().equals(nameCampo)) {
        		try {               
        			campo.setAccessible(true); //Necessário por conta do encapsulamento das variáveis (private)
        			Object objeto = ReflectionUtils.getField(campo, entidade);
        			if(objeto instanceof String) {
        				return (String) objeto;
        			}
        			if(objeto instanceof Long) {
        				return ((Long) objeto).toString();
        			}
        			if(objeto instanceof BigDecimal) {
        				return ((BigDecimal) objeto).toString();
        			}
        		} catch (Exception e) {
        			e.printStackTrace();
        		}        		
        	}
        }
        return "";
    }
	
	public static <E> E preencherEntidadeDinamicamente(Class<E> clazz, Map<String, Object> camposDados) {
		ObjectMapper objectMapper = new ObjectMapper();
				
		E entidadeDestino = (E) objectMapper.convertValue(camposDados, clazz);
			
		return entidadeDestino;
	}

}
