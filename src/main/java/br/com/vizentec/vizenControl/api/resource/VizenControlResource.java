package br.com.vizentec.vizenControl.api.resource;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.vizentec.vizenControl.api.dto.AutorizacaoDto;
import br.com.vizentec.vizenControl.api.dto.MovimentoDto;
import br.com.vizentec.vizenControl.api.dto.PagamentoDto;
import br.com.vizentec.vizenControl.api.model.Movimento;
import br.com.vizentec.vizenControl.api.model.Pagamento;
import br.com.vizentec.vizenControl.api.model.Usuario;
import br.com.vizentec.vizenControl.api.service.VizenControlService;
import io.swagger.annotations.ApiParam;

@RestController
public class VizenControlResource {
	
	private DateTimeFormatter dtfComp = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
	
	@Autowired
	private VizenControlService service;

	@GetMapping("/extrato/{idDispositivo}/{dataInicio}/{dataFim}")
	public ResponseEntity<List<Movimento>> consultarExtrato(@PathVariable("idDispositivo") String idDispositivo, @PathVariable("dataInicio") String dataInicio, @PathVariable("dataFim") String dataFim){
		try {
			LocalDateTime dateTimeInicio = LocalDateTime.parse(dataInicio, dtfComp);
			LocalDateTime dateTimeFinal = LocalDateTime.parse(dataFim, dtfComp);
			List<Movimento> lista = service.consultarExtrato(idDispositivo, dateTimeInicio, dateTimeFinal);
			
			if(lista.isEmpty()) {
				return ResponseEntity.notFound().build();
			}
			
			return ResponseEntity.status(HttpStatus.OK).body(lista);
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}
	
	@PostMapping("/movimentacao")
	public ResponseEntity<?> movimentacao(@RequestBody MovimentoDto dto){
		try {
			LocalDateTime dateTime = LocalDateTime.parse(dto.getData(), dtfComp);		

			Movimento movimento = service.movimentacao(dto.getIdDispositivo(), dto.getIdProduto(), dateTime, dto.getValor());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(movimento);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@PostMapping("/autorizacao")
	public ResponseEntity<?> autorizacao(@RequestBody AutorizacaoDto dto){
		try {
			LocalDateTime dateTime = LocalDateTime.parse(dto.getData(), dtfComp);
			
			Usuario usuario = service.autorizacao(dto.getIdDispositivo(), dateTime, dto.getNome());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(usuario);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@PostMapping("/pagamento")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> pagamento(@RequestBody PagamentoDto dto){
		try {
			Pagamento pagamento = service.pagamento(dto.getIdDispositivo(), dto.getValor());
			
			return ResponseEntity.status(HttpStatus.CREATED).body(pagamento);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}

	protected BigDecimal obterValorByBigDecimal(String valor) {BigDecimal valorBigDecimal = (new BigDecimal(valor)).setScale(2);
		return valorBigDecimal;
	}

}
