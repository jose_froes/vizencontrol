package br.com.vizentec.vizenControl.api.repository;

import java.time.LocalDateTime;
import java.util.List;

import br.com.vizentec.vizenControl.api.model.Movimento;

public interface MovimentacaoRepository extends VizenControlBaseRepository<Movimento> {
	
	List<Movimento> findAllByIdDispositivoAndDataAfterAndDataBefore(String idDispositivo, LocalDateTime dateTimeInicio, LocalDateTime dateTimeFinal);

}
